import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

class GlycosampleList extends PolymerElement {
  static get template() {
    return html`
<style>
/* shadow DOM styles go here */:
/* move out to css file? */:

host {
    display: inline - block;
}
iron - icon {
    fill: var (--icon - toggle - color, rgba(0, 0, 0, 0));
    stroke: var (--icon - toggle - outline - color, currentcolor);
}: host([pressed]) iron - icon {
    fill: var (--icon - toggle - pressed - color, currentcolor);
}

table.TwoWayBack {
    border - collapse: collapse;
    text - align: left;
    line - height: 1.5;
    border - top: 10 px solid gray;
    border - bottom: 1 px solid gray;
    align: center;
    vertical - align: middle;
}
table.TwoWayBack thead th {
    width: 150 px;
    padding: 10 px;
    font - weight: bold;
    vertical - align: middle;
    color: #fff;
    background: #04162e;
}
table.TwoWayBack td {
  width: 650px;
  padding: 10px;
  vertical-align: middle;
  text-align: left;
}

table.TwoWayBack tr:hover {
  background-color: palegreen;
}

table.TwoWayBack tr {
  border-bottom-color: green;
  vertical-align: middle;
}

td {
  word-break:break-all;
  text-align: left;
  vertical-align: middle;
}

table.TwoWayBack tr:nth-child(even) {
  background-color: gainsboro;
}

table.TwoWayBack img {
  width:300px;
}

body h1, h2, h3, h4, iframe, footer {
  text-align: center;
}

iframe {
  border: 1px darkgreen solid;
}

.green-bord {
  background-color: # 82 ae46;
    color: #00552e;
}

.green-bord-black {
  background-color: # cee4ae;
    color: black;
}

div {
    align: center;
    text - align: center;
    width: 100 % ;
    vertical - align: middle;
}
</style>

<!-- GlycoNavi SampleList Component -->
  <iron-ajax auto url="https://sparqlist.glyconavi.org/api/glycosample_list" handle-as="json" last-response="{{sampleids}}"></iron-ajax>
  <div>
    <table class="TwoWayBack">
      <thead>
        <tr>
          <th scope="cols">GlycoSample IDs</th>
        </tr>
      </thead>
      <tbody>
        <template is="dom-repeat" items="{{sampleids}}">
          <tr>
            <th scope="row"><a href="GlycoSampleEntry.html?id={{item.id}}" target="_blank">[[item.id]]</a></th>
          </tr>
        </template>
      </tbody>
    </table>
  </div>
   `;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('glycosample-list', GlycosampleList);
