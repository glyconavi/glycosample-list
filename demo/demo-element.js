import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../glycosample-list.js';

class DemoElement extends PolymerElement {
  static get template() {
    return html`
      <style>
  :host {
font-family: sans-serif;
--icon-toggle-color: lightgrey;
--icon-toggle-outline-color: black;
--icon-toggle-pressed-color: red;
}
      </style>
      
      <h3>Glycosample List Demo</h3>
      <glycosample-list></glycosample-list>
        
    `;
  }
}
customElements.define('demo-element', DemoElement);
