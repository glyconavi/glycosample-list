# GlycoNavi Components 

You'll need to install some command-line tools to manage dependencies and to run the demo.

1.  Download and install Node version 6.x or 7.x from [https://nodejs.org/](https://nodejs.org/). Node includes the node package manager command, `npm`.

2.  Install `bower` and the Polymer CLI:

        npm install -g bower polymer-cli

3.  Clone this repo:

        https://gitlab.com/glyconavi/<component name>.git
        
4.  Change directory to your local repo and install dependencies with `bower`:

        cd <component name>
        polymer install
        
5.  To preview your element, run the Polymer development server from the repo directory:

        polymer serve

  Follow the instructions on command line.
